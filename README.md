# Instructions to run the Dellent Test

- Run `pip install -r requirements/requirements.txt`

### Run the tests

- On Linux/Mac `make unittests`  or  `nosetests --with-specplugin`

### Run a specific test
```Shell
nosetests 'tests.dellent_tests:PaginationTests.test_return_error_message_if_current_page_is_bigger_than_total_pages'
```

# Opinions (o teclado em uso esta em GB, os acentos nao existem e peco desculpa pelo mesmo)

- Achei o exercicio interessante mas algo confuso no que toca entre os boundaries e o around. Penso ter conseguido chegar a lista final de resultados atraves do `get_list_values_message`. Ao testar, conseguirao ver que retorna os resultados com excepcao dos ...  mas os calculos, a partida, estao la
- Tenho apenas alguns pontos a acrescentar, acho que foi um bom exercicio ao nivel de pensamento mas ao nivel de solucao final, na minha optica, nao faria desta forma, seja em Java ou em Python, sendo um python developer diria que isto passaria para uma `templatetag` em Django e trabalharia em conformidade com Ajax e retornaria resultados mais exactos e onde strings estaticas eram tratadas no HTML e nao em python puro.

- Tenho um exemplo funcional de uma templatetag com HTML e python que resultaria em paginacao.

- Python:

```Python
@register.assignment_tag
def get_previous_pages(page, max_offset=2):
    page_number = page.number

    if page_number == 1:
        return []

    start = page_number - max_offset
    if start < 1:
        start = 1

    if start - 1 == 1:
        sequence = [1]
    else:
        sequence = [1, None]

    sequence += range(start, page_number)

    start_index = len(sequence) - 1 - sequence[::-1].index(1)
    sequence = sequence[start_index:]

    return sequence

```

- HTML

```HTML
 <ul class="af-pagination">
        {% if page_obj.has_previous %}
            <li class="arrow">
                <label class="margin margin-none" for="page-{{ page_obj.previous_page_number }}" ><i class="icon icon-auto-size icon-arrow-rounded-left"></i></label>
            </li>
        {% endif %}
        {% limit_page_range page_obj as page_range %}
        {% for page in page_range %}
            {% if page %}
                <li {% if page == page_obj.number %}class="current"{% endif %}>
                    <label class="margin margin-none " for="page-{{ page }}">{{ page }}</label><input id="page-{{ page }}" class="hide"  type="radio" name="page" value="{{ page }}">
                </li>
            {% else %}
                <li><a href="javascript:void(0)">...</a></li>
            {% endif %}
        {% endfor %}
        {% if page_obj.has_next %}
            <li class="arrow">
                <label class="margin margin-none" for="page-{{ page_obj.next_page_number }}"><i class="icon icon-auto-size icon-arrow-rounded-right"></i></label>
            </li>
        {% endif %}
    </ul>
```

# Opinioes finais

- Durante o tempo que tive de fazer o teste, nao consegui estar a 100% visto estar num periodo de transicao, contudo nao imprimindo a string pedida, o resultado de lista para a string, retorna os valores pedidos (ate a data dos testes). Peco desculpa pela falta de tempo enquanto me encontro em periodo de transicao mas obviamente que ha solucoes mais rapidas.