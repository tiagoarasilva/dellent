class Pagination(object):
    message = None

    def __init__(self, current_page, total_pages, boundaries, around):
        self.current_page = current_page
        self.total_pages = total_pages
        self.boundaries = boundaries
        self.around = around

    def get_list_of_values(self):
        '''
        Values passed through the __init__
        :return: list
        '''
        return [self.current_page, self.total_pages, self.boundaries, self.around]

    @property
    def has_prev(self):
        """
        Checks if there is a previous page
        :return: bool
        """
        return self.current_page > 1

    @property
    def has_next(self):
        '''
        Checks if there is a next page
        :return: bool
        '''
        return self.current_page < self.total_pages

    def get_list(self):
        '''
        Gets the list of pages with no data treatment
        :return: list
        '''
        return [x for x in range(1, self.total_pages + 1)]

    def get_boundaries_start(self):
        '''
        Returns the boundaries from the beginning of the list
        :return: list
        '''
        return self.get_list()[:self.boundaries]

    def get_boundaries_end(self):
        '''
        Returns the boundaries from the end of the list
        :return: list
        '''
        if self.boundaries > 1:
            return self.get_list()[-self.boundaries:]
        return self.get_list()[-self.boundaries - 1:]

    def get_around(self):
        '''
        Get the around values from the current_page (left and right)
        :return: list
        '''
        arounds = []

        if self.current_page >= self.total_pages:
            return self.get_list()

        pagination = self.get_list()
        left_limit = self.current_page + self.around

        if self.around != 0:
            right_limit = (self.current_page - self.around) - 1
        else:
            right_limit = self.current_page - self.around

        for value in pagination[right_limit:self.current_page]:
            arounds.append(value)

        for value in pagination[self.current_page:left_limit]:
            arounds.append(value)
        return arounds

    def get_string_builder_for_left_side(self):
        '''
        Builds the string based on the result list obtained by the boundaries and
        from the around
        :return: str
        '''
        message_list = list(set().union(
            self.get_boundaries_start(),
            self.get_around()
        ))
        return message_list

    def get_string_builder_for_right_side(self):
        '''
        Builds the string based on the result list obtained by the boundaries and
        from the around
        :return: str
        '''
        message_list_left = list(set().union(
            self.get_boundaries_start(),
            self.get_around()
        ))
        right_list = []

        for value in self.get_boundaries_end():
            if value not in message_list_left:
                right_list.append(value)
        return right_list

    def get_list_values_message(self):
        '''
        Compares and checks if lists matches
        :return: str
        '''
        values_to_be_dots = []
        left = self.get_string_builder_for_left_side()
        right = self.get_string_builder_for_right_side()
        message_concatenated = left + right

        for value in self.get_list():
            if value not in message_concatenated:
                values_to_be_dots.append(value)

        pagination = [x for x in self.get_list() if x not in values_to_be_dots]

        return pagination

    def get_pagination(self):
        for value in self.get_list_of_values():
            if value < 0:
                self.message = u'There is a negative value, please change it'
                return self.message

        if self.current_page > self.total_pages:
            self.message = u'The current page is superior to the total of pages'
            return self.message

        print self.get_list_values_message()
        return self.get_list_values_message()
