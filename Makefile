clean: clean_pyc
clean_pyc:
	find . -type f -name "*.pyc" -delete || true

unittests:
	nosetests --nocapture --with-specplugin
