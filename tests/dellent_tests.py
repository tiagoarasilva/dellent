import unittest

from dellent.pagination import Pagination


class PaginationTests(unittest.TestCase):

    def _generate_data_for_test(self, current_page, total_pages, boundaries, around):
        pagination = Pagination(current_page, total_pages, boundaries, around)
        return pagination.get_pagination()

    def test_return_list_of_values_passed_through_counstructor(self):
        pagination = Pagination(12, 12, 12, 12)

        result = pagination.get_list_of_values()

        self.assertEqual(result, [12, 12, 12, 12])

    def test_return_error_message_if_current_page_is_bigger_than_total_pages(self):
        result = self._generate_data_for_test(14, 2, 1, 5)

        self.assertEqual(u'The current page is superior to the total of pages', result)

    def test_fail_if_current_page_value_negative(self):
        result = self._generate_data_for_test(-1, 2, 1, 5)

        self.assertEqual(u'There is a negative value, please change it', result)

    def test_fail_if_total_pages_value_negative(self):
        result = self._generate_data_for_test(1, -2, 1, 5)

        self.assertEqual(u'There is a negative value, please change it', result)

    def test_fail_if_boundaries_value_negative(self):
        result = self._generate_data_for_test(1, 2, -1, 5)

        self.assertEqual(u'There is a negative value, please change it', result)

    def test_fail_if_around_value_negative(self):
        result = self._generate_data_for_test(1, 2, 1, -5)

        self.assertEqual(u'There is a negative value, please change it', result)

    def test_if_one_or_more_negative_values_fails_the_creation_of_pagination(self):
        result = self._generate_data_for_test(-1, 2, 1, -5)

        self.assertEqual(u'There is a negative value, please change it', result)

    def test_first_combination_of_values(self):
        result = self._generate_data_for_test(4, 5, 1, 0)

        self.assertEqual(result, [1, 4, 5])

    def test_second_combination_of_values(self):
        result = self._generate_data_for_test(4, 10, 2, 2)

        self.assertEqual(result, [1, 2, 3, 4, 5, 6, 9, 10])

    def test_third_combination_of_values(self):
        result = self._generate_data_for_test(5, 10, 2, 2)

        self.assertEqual(result, [1, 2, 3, 4, 5, 6, 7, 9, 10])

    def test_boundaries_values_for_the_left_side(self):
        pagination = Pagination(4, 10, 2, 2)

        result = pagination.get_boundaries_start()

        self.assertEqual([1, 2], result)

    def test_boundaries_values_for_the_right_side(self):
        pagination = Pagination(4, 10, 2, 2)

        result = pagination.get_boundaries_end()

        self.assertEqual([9, 10], result)

    def test_around_numbers_based_on_position_of_current_page(self):
        pagination = Pagination(4, 10, 2, 2)

        result = pagination.get_around()

        self.assertEqual([2, 3, 4, 5, 6], result)



